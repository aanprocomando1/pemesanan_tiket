Ext.define('Pemesanan_tiket.store.Personnel', {
    extend: 'Ext.data.Store',

    alias: 'store.personnel',
    storeId:'personnel',
    autoLoad: true,
    autoSync: true,


    fields: [
       'user_id', 'name', 'email', 'phone', 'tujuan', 'waktu'
    ],

    proxy: {
        type: 'jsonp',
        api: {
         read: "http://localhost/MyApp_php/ReadPersonnel_Pemesanan_Tiket.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});
