Ext.define('Pemesanan_tiket.view.form.Register', {
    extend: 'Ext.form.Panel',
    xtype: 'register',
    controller: 'login',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Password'
        
    ],
    shadow: true,
    cls: 'demo-solid-background',
    id: 'formregister',
    items: [
        {
            xtype: 'fieldset',
            id: 'fieldsetregister',
            title : 'Isi Form Register',
            instructions: 'Please enter the information above.',
            defaults: {
                labelWidth: '35%'
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'nama',
                    label: 'Nama',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'textfield',
                    name: 'nohp',
                    label: 'No Handphone',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'textfield',
                    name: 'email',
                    label: 'Email',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'passwordfield',
                    revealable: true,
                    name : 'password',
                    label: 'Password',
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'passwordfield',
                    revealable: true,
                    name : 'confirmpassword',
                    label: 'Confirm Password',
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'textfield',
                    name: 'alamat',
                    label: 'Alamat',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'datepickerfield',
                    destroyPickerOnHide: true,
                    name: 'tanggal',
                    label: 'Tanggal Lahir',
                    value: new Date(),
                    picker: {
                        yearFrom: 1990
                    }
                }
            ]
        },
        {
            xtype: 'container',
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                {
                    text: 'Simpan',
                    ui: 'action',
                    handler: 'onRegister'
                    
                },
                {
                    text: 'Reset',
                    ui: 'action',
                    style: 'background-color:#E61607;margin:14px;color: white',
                    handler: function(){
                        Ext.getCmp('formregister').reset();
                    }
                },
            ]
        },
    ]
});