/**
 * This view is an example list of people.
 */
Ext.define('Pemesanan_tiket.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'Pemesanan_tiket.store.Personnel'
    ],

    title: 'Riwayat Pesanan',

    store: {
        type: 'personnel'
    },

    columns: [
        { text: 'Name',  dataIndex: 'name', width: 200 },
        { text: 'Email', dataIndex: 'email', width: 230 },
        { text: 'Phone', dataIndex: 'phone', width: 150 },
        { text: 'Tujuan', dataIndex: 'tujuan', width: 230 },
        { text: 'Waktu', dataIndex: 'waktu', width: 130 }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
