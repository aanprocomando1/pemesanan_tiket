Ext.define('Pemesanan_tiket.view.main.FormTiket', {
    extend: 'Ext.form.Panel',
    xtype: 'myform',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Number',
        'Ext.field.Spinner',
        'Ext.field.Password',
        'Ext.field.Email',
        'Ext.field.Url',
        'Ext.field.DatePicker',
        'Ext.field.Select',
        'Ext.field.Hidden',
        'Ext.field.Radio'
    ],
    shadow: true,
    cls: 'demo-solid-background',
    xtype: 'formpanel',
    id: 'basicform',
    items: [
        {
            xtype: 'fieldset',
            id: 'fieldset1',
            title: 'Pemesanan Tiket',
            defaults: {
                labelWidth: '35%'
            },
            items: [
                {
                    xtype: 'textfield',
                    name: 'name',
                    label: 'Name',
                    placeHolder: 'Masukkan Nama Anda',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                {
                    xtype: 'textfield',
                    name: 'no_handphone',
                    label: 'No Handphone',
                    placeHolder: 'Masukkan No Hp Anda',
                    autoCapitalize: true,
                    required: true,
                    clearIcon: true
                },
                /*{
                    xtype: 'passwordfield',
                    revealable: true,
                    name : 'password',
                    label: 'Password',
                    clearIcon: true
                },
                {
                    xtype: 'emailfield',
                    name: 'email',
                    label: 'Email',
                    placeHolder: 'me@sencha.com',
                    clearIcon: true
                },
                {
                    xtype: 'urlfield',
                    name: 'url',
                    label: 'Url',
                    placeHolder: 'http://sencha.com',
                    clearIcon: true
                },
                {
                    xtype: 'spinnerfield',
                    name: 'spinner',
                    label: 'Spinner',
                    minValue: 0,
                    maxValue: 10,
                    clearable: true,
                    stepValue: 1,
                    cycle: true
                },
                {
                    xtype: 'checkboxfield',
                    name: 'cool',
                    label: 'Cool',
                    reference: 'cooll',
                    platformConfig: {
                        '!desktop': {
                            bodyAlign: 'end'
                        }
                    }
                },/*
                {
                    xtype: 'datepickerfield',
                    destroyPickerOnHide: true,
                    name: 'date',
                    label: 'Start Date',
                    value: new Date(),
                    picker: {
                        yearFrom: 1990
                    }
                },*/
                {
                    xtype: 'selectfield',
                    name: 'Kota Keberangkatan',
                    label: 'Kota Keberangkatan',
                    options: [
                        {
                            text: '---',
                            value: '---'
                        },
                        {
                            text: 'Pekanbaru',
                            value: 'pekanbaru'
                        },
                        {
                            text: 'Jakarta',
                            value: 'jakarta'
                        },
                        {
                            text: 'Yogyakarta',
                            value: 'yogyakarta'
                        },
                        {
                            text: 'Surabaya',
                            value: 'surabaya'
                        }
                    ]
                },
                {
                    xtype: 'selectfield',
                    name: 'Kota Tujuan',
                    label: 'Kota Tujuan',
                    options: [
                        {
                            text: '---',
                            value: '---'
                        },
                        {
                            text: 'Pekanbaru',
                            value: 'pekanbaru'
                        }, 
                        {
                            text: 'Jakarta',
                            value: 'jakarta'
                        },
                        {
                            text: 'Yogyakarta',
                            value: 'yogyakarta'
                        },
                        {
                            text: 'Surabaya',
                            value: 'surabaya'
                        }
                    ]
                },
                {
                    xtype: 'selectfield',
                    name: 'Waktu',
                    label: 'Waktu',
                    options: [
                        {
                            text: '---',
                            value: '---'
                        },
                        {
                            text: '08:00',
                            value: '08:00'
                        }, 
                        {
                            text: '10:00',
                            value: '10:00'
                        },
                        {
                            text: '13:00',
                            value: '13:00'
                        },
                        {
                            text: '16:00',
                            value: '16:00'
                        }
                    ]
                },
                {
                    xtype: 'spinnerfield',
                    name: 'spinner',
                    label: 'Jumlah Penumpang',
                    minValue: 0,
                    maxValue: 7,
                    clearable: true,
                    stepValue: 1,
                    cycle: true
                },/*
                {
                    xtype: 'sliderfield',
                    name: 'slider',
                    label: 'Slider'
                },
                {
                    xtype: 'togglefield',
                    name: 'toggle',
                    label: 'Toggle'
                },
                {
                    xtype: 'textareafield',
                    name: 'bio',
                    label: 'Bio'
                }*/
            ]
        },
        {
                    xtype: 'datepickerfield',
                    destroyPickerOnHide: true,
                    name: 'date',
                    label: 'Tanggal Keberangkatan',
                    value: new Date(),
                    picker: {
                        yearFrom: 2020
                    }
                },/*
        {
            xtype: 'fieldset',
            id: 'fieldset2',
            title: 'Favorite color',
            platformConfig: {
                '!desktop': {
                    defaults: {
                        bodyAlign: 'end'
                    }
                }
            },
            defaults: {
                xtype: 'radiofield',
                labelWidth: '35%'
            },
            items: [
                {
                    name: 'color',
                    value: 'red',
                    label: 'Red'
                },
                {
                    name: 'color',
                    label: 'Blue',
                    value: 'blue'
                },
                {
                    name: 'color',
                    label: 'Green',
                    value: 'green'
                },
                {
                    name: 'color',
                    label: 'Purple',
                    value: 'purple'
                }
            ]
        },*/
        {
            xtype: 'container',
            bind : {
                        hidden: '{!cooll.checked}'
                    },
            defaults: {
                xtype: 'button',
                style: 'margin: 1em',
                flex: 1
            },
            layout: {
                type: 'hbox'
            },
            items: [
                /*{
                    text: 'Disable fields',
                    ui: 'action',
                    scope: this,
                    hasDisabled: false,
                    handler: function(btn){
                        var fieldset1 = Ext.getCmp('fieldset1'),
                            fieldset2 = Ext.getCmp('fieldset2');

                        if (btn.hasDisabled) {
                            fieldset1.enable();
                            fieldset2.enable();
                            btn.hasDisabled = false;
                            btn.setText('Disable fields');
                        } else {
                            fieldset1.disable();
                            fieldset2.disable();
                            btn.hasDisabled = true;
                            btn.setText('Enable fields');
                        }
                    }
                },*/
                {
                    text: 'Confirm',
                    ui: 'action',
                    handler: function() {
                    Ext.Msg.confirm("Confirmation", "Apa anda yakin ingin mencari tiket?", Ext.emptyFn);
                    }
                },
                {
                    text: 'Reset',
                    style: 'background-color:#E61607;margin:14px;color: white',
                    ui: 'action',
                    handler: function(){
                        Ext.getCmp('basicform').reset();
                    }
                }
                ]
        }
    ]
});